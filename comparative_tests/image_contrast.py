# import PIL
import tensorflow as tf
import tensorflow.keras as K
import numpy as np


def label_function(image):
    image = np.reshape(image, (-1, 1))
    # image = PIL.Image.fromarray(image)
    # image_stat = PIL.ImageStat.Stat(image)
    # if image_stat.mean[0] != 0:
    #    return image_stat.rms[0] / image_stat.mean[0]
    # else:
    #    return -1
    rms = np.sqrt(np.mean(np.square(image)))
    mean = np.mean(image)

    if mean != 0:
        return rms / mean
    else:
        return -1


model = K.models.Sequential(
    [K.layers.Dense(100, activation="sigmoid", input_shape=(784,)), K.layers.Dense(1)]
)
