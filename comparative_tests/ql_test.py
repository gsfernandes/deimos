import ql
import image_contrast
import tensorflow.keras as K


image_contrast.model.compile(
    optimizer=K.optimizers.Adam(5000),
    loss="mean_squared_error",
    metrics=["mean_absolute_error"],
)
modhand = ql.ModelHandler(
    image_contrast.model, image_contrast.label_function, K.backend.get_session()
)
modhand.compile()

AT = ql.PropAutoTune(0.000000001, 0.000000001)
AT(modhand)
