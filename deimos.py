from cad.hull import hull as hullCreate

# from ql import DataTape
import subprocess
import os
from math import sqrt
import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow import keras as K
import asciiplotlib as apl



loa = 228.31
breadth = 32.34
draught = 9.2

FNULL = open(os.devnull, "w")
quiet_args = {"stdout": FNULL, "stderr": subprocess.STDOUT}


def hullMesh(profile, length, breadth, draught):
    os.chdir("cad/")
    subprocess.run(["rm", "hull.iges", "hull.msh"], **quiet_args)
    hullCreate(profile, length, breadth, draught)
    subprocess.run(["gmsh", "-3", "-format", "msh2", "hull.geo"], **quiet_args)
    subprocess.run(["mv", "hull.msh", "../"], **quiet_args)
    os.chdir("../")


def hullFoamComplete():
    os.chdir("cfd/")
    subprocess.run(["./Allclean"], **quiet_args)
    subprocess.run(["mv", "../hull.msh", "./"], **quiet_args)
    subprocess.run(["gmshToFoam", "hull.msh"], **quiet_args)
    subprocess.run(["simpleFoam"], **quiet_args)
    os.chdir("../")
    df = pd.read_csv(
        "cfd/postProcessing/resistance/0/forces.dat",
        sep="[()\t\s]+",
        comment="#",
        engine="python",
    )
    df.columns = list(range(20))
    x = np.linspace(0, df[4].shape[0] - 1, df[4].shape[0])
    y = df[4]
    # fig = apl.figure()
    # fig.plot(x, y, label="time", width=140, height=40)
    # fig.show()

    return df[4]


def hullFoam():
    hFC = hullFoamComplete()
    return hFC[hFC.shape[0] - 1]


def resistance(profile):
    if isinstance(profile, np.ndarray) and len(profile.shape) > 2:
        return [resistance(_) for _ in profile]
    else:
        hullMesh(profile, loa, breadth, draught)
        return hullFoam()


def sampling(args):
    """Reparameterization trick by sampling from an isotropic unit Gaussian.
	# Arguments
		args (tensor): mean and log of variance of Q(z|X)
	# Returns
		z (tensor): sampled latent vector
	"""
    z_mean, z_log_var = args
    batch = K.backend.shape(z_mean)[0]
    dim = K.backend.int_shape(z_mean)[1]
    # by default, random_normal has mean = 0 and std = 1.0
    epsilon = K.backend.random_normal(shape=(batch, dim))
    return z_mean + K.backend.exp(0.5 * z_log_var) * epsilon


def encode(units, input1, input2):
    # VAE model = encoder + decoder
    # build encoder model
    x = K.layers.Dense(units[1], activation="relu")(input1)
    z_mean = K.layers.Dense(units[2], name="z_mean")(x)
    z_log_var = K.layers.Dense(units[2] + 1, name="z_log_var")(x)
    bn = K.layers.BatchNormalization()(input2)
    z_concat = K.layers.Concatenate(name="z_concat")([z_mean, bn])
    z = K.layers.Lambda(sampling, name="z")([z_concat, z_log_var])
    encoder = K.models.Model([input1, input2], [z_concat, z_log_var, z], name="encoder")
    encoder.summary()
    return encoder


def decode(units):
    latent_inputs = K.layers.Input(shape=(units[2] + 1,), name="z_sampling")
    x = K.layers.Dense(units[1], activation="relu")(latent_inputs)
    outputs = K.layers.Dense(units[0], activation="sigmoid", name="output")(x)
    decoder = K.models.Model(latent_inputs, outputs, name="decoder")
    return decoder


def model(dimensions=[25, 512, 5]):
    batch_size = 32

    input1 = K.layers.Input(shape=dimensions[0], name="bezier")
    input2 = K.layers.Input(shape=1, name="resistance")
    encoder = encode(dimensions, input1, input2)
    decoder = decode(dimensions)

    outputs = decoder(encoder([input1, input2]))
    vae = K.models.Model([input1, input2], outputs, name="vae")

    def vae_loss(x, x_decoded_mean):
        reconstruction_loss = K.losses.binary_crossentropy(x, x_decoded_mean)
        reconstruction_loss *= dimensions[0]

        z_log_var = encoder.get_layer("z_log_var").output
        z_concat = encoder.get_layer("z_concat").output

        kl_loss = 1 + z_log_var - K.backend.square(z_concat) - K.backend.exp(z_log_var)
        kl_loss = K.backend.sum(kl_loss, axis=-1)
        kl_loss *= -0.5

        vae_loss = K.backend.mean(reconstruction_loss + kl_loss)
        return vae_loss

    # vae.add_loss(vae_loss)
    vae.compile(optimizer=K.optimizers.Adam(0.01), loss=vae_loss)
    return vae


def design_seed(dimensions, seeds):
    hulls = np.random.random([seeds, dimensions[0]])
    sqrt_dim = int(sqrt(dimensions[0]))
    resistances = np.empty([0, sqrt_dim, sqrt_dim])
    for _ in range(seeds):
        reshaped_hull = np.reshape(hulls[_], [sqrt_dim, sqrt_dim])
        resistances = np.append(resistances, resistance(reshaped_hull))

    return hulls, resistances


dimensions = [64, 512, 2]

no_hulls = 10
sqrt_dim = int(sqrt(dimensions[0]))
hull_pool = np.empty([0, dimensions[0]])
resistance_pool = np.empty([0])

hulls, resistances = design_seed(dimensions, no_hulls)
hull_pool = np.append(hull_pool, hulls, axis=0)
resistance_pool = np.append(resistance_pool, resistances)
mean_resistance = [np.mean(resistances)]
vae = model(dimensions)

_ = 0
while True:

    print("\n\n", _, "\n\n")
    print("resistances:", mean_resistance)

    x = np.linspace(0, len(mean_resistance), len(mean_resistance))
    y = mean_resistance
    fig = apl.figure()
    fig.plot(x, y, label="iteration", width=63, height=21)
    fig.show()

    vae.fit({"bezier": hull_pool, "resistance": resistance_pool}, hull_pool, verbose=0, epochs=3999)
    vae.fit({"bezier": hull_pool, "resistance": resistance_pool}, hull_pool, verbose=2, epochs=1)
    # print(type(resistances))
    hulls = np.reshape(
        vae.predict({"bezier": hulls, "resistance": resistances * 0.9}),
        [no_hulls, sqrt_dim, sqrt_dim],
    )
    resistances = np.array(resistance(hulls))
    hulls = np.reshape(
        hulls,
        [no_hulls, dimensions[0]],
    )
    hull_pool = np.append(hull_pool, hulls, axis=0)
    resistance_pool = np.append(resistance_pool, resistances)
    mean_resistance.append(np.mean(resistances))
    _ += 1

print(mean_resistance)
