FROM ubuntu:bionic



RUN apt-get update && apt-get install -y \
		sudo \
		wget \
		software-properties-common ;\
		rm -rf /var/lib/apt/lists/*

RUN useradd --user-group --create-home --shell /bin/bash notroot ;\
	echo "notroot ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers



# Blank out .bashrc so that it gets run in non interactive scripts
RUN echo "" > /home/notroot/.bashrc



# OpenFOAM v6
RUN sh -c "wget -O - http://dl.openfoam.org/gpg.key | apt-key add -" ;\
    add-apt-repository http://dl.openfoam.org/ubuntu ;\
	  apt-get update ;\
	  apt-get install -y --no-install-recommends openfoam6 ;\
    echo "source /opt/openfoam6/etc/bashrc" >> /home/notroot/.bashrc ;\
	  echo "export OMPI_MCA_btl_vader_single_copy_mechanism=none" >> /home/notroot/.bashrc



# Conda
RUN apt-get -qq update && apt-get -qq -y install curl bzip2 \
    && curl -sSL https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -o /tmp/miniconda.sh \
    && bash /tmp/miniconda.sh -bfp /usr/local \
    && rm -rf /tmp/miniconda.sh \
    && conda install -y python=3 \
    && conda update conda \
    && apt-get -qq -y remove curl bzip2 \
    && apt-get -qq -y autoremove \
    && apt-get autoclean \
    && rm -rf /var/lib/apt/lists/* /var/log/dpkg.log \
    && conda clean --all --yes
ENV PATH /opt/conda/bin:$PATH
RUN conda create -n conda python=3.7
RUN echo "source activate conda" >> /home/notroot/.bashrc
ENV PATH /opt/conda/envs/conda/bin:$PATH



# PythonOCC
RUN conda install -n conda -y -c conda-forge -c pythonocc -c dlr-sc -c tpaviot oce pythonocc-core && conda clean --all --yes



# Gmsh
RUN conda install -c conda-forge gmsh && conda clean --all --yes



# Numpy
RUN conda install -n conda numpy && conda clean --all --yes



# OpenMPI
RUN apt-get update -y && \
    apt-get install -y --no-install-recommends sudo apt-utils && \
    apt-get install -y --no-install-recommends openssh-server \
        python-dev python-numpy python-pip python-virtualenv python-scipy \
        gcc gfortran libopenmpi-dev openmpi-bin openmpi-common openmpi-doc binutils

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

RUN pip install --upgrade pip
USER notroot
RUN  pip install --user -U setuptools \
    && pip install --user mpi4py

USER root
RUN rm -fr /home/notroot/.openmpi && mkdir -p /home/notroot/.openmpi
ADD default-mca-params.conf /home/notroot/.openmpi/mca-params.conf
RUN chown -R notroot:notroot /home/notroot/.openmpi



# Pandas
RUN conda install -n conda pandas && conda clean --all --yes



# Tini container init
RUN apt-get install -y curl grep sed dpkg && \
    TINI_VERSION=`curl https://github.com/krallin/tini/releases/latest | grep -o "/v.*\"" | sed 's:^..\(.*\).$:\1:'` && \
    curl -L "https://github.com/krallin/tini/releases/download/v${TINI_VERSION}/tini_${TINI_VERSION}.deb" > tini.deb && \
    dpkg -i tini.deb && \
    rm tini.deb && \
    apt-get clean



# Pandas
RUN conda install -n conda -c anaconda tensorflow-mkl=1.15 && conda clean --all --yes



# Libxmu6 for gmsh
RUN apt-get update && apt-get install -y libxmu6



USER notroot
WORKDIR /home/notroot/



# Deimos
# COPY --chown=notroot:notroot ./ql/ /home/notroot/ql/
RUN sudo bash -ic "conda activate conda && pip install asciiplotlib"
COPY --chown=notroot:notroot docker_entrypoint.sh /home/notroot/
# COPY --chown=notroot:notroot ./default-mca-params.conf /home/notroot/
# COPY --chown=notroot:notroot ./cad/ /home/notroot/cad/
# COPY --chown=notroot:notroot ./cfd/ /home/notroot/cfd/
# COPY --chown=notroot:notroot deimos.py /home/notroot/
ENV TF_CPP_MIN_LOG_LEVEL=2
ENTRYPOINT ["/usr/bin/tini", "--", "/bin/bash", "docker_entrypoint.sh"]